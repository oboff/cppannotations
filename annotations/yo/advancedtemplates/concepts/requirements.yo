The bodies of tt(requires) declarations contain define constraints to apply to
template parameters. There are four types of requirements:
    itemization(
    it() em(Simple requirements): constraints on em(facilities) (like
        requiring the availability of tt(operator+));
    it() em(Type requirements): requiring the availability of (sub)types (like 
        the sub-type tt(value_type)) which must be available when using
        standard tt(push_back) functions;
    it() em(Compound requirements): requiring types resulting from applying
        operators or returned by called (member) functions;
    it() em(Nested requirements): defining concepts in terms of already
        existing concepts. 
    )
    
Constraints must be compile-time verifiable.

When multiple constraints are specified, they must em(all) be compile-time
verifiable, and an actual type is only accepted by the compiler if all
requirements could be satisfied. 

COMMENT(
Multiple constraints
can be defined by specifying multiple requirements in
tt(requires)-specifications and/or by 
combining constraints using the boolean operators tt(and, or) and tt(not).
END)
