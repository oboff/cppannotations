#include <utility>
#include <concepts>
#include <string>

template <typename Type>
concept ConstDereferenceable =
    requires(Type type)
    {
        { *type } -> std::same_as<int const &>;
//        { *type } -> std::convertible_to<int const &>;
    };
//=

template <typename Type>
concept Dereferenceable =
    requires(Type type)
    {
        { *type } -> std::same_as<int &>;
    };

//iterable
struct Iterable
{
    Iterable &operator++();
    Iterable operator++(int);

    int const &operator*() const;
    int &operator*();
};
//=

template <typename Type>
concept InIterator =
    ConstDereferenceable<Type>;

template <InIterator Type>
void inFun(Type tp)
{}


template <typename Type>
concept OutIterator =
    Dereferenceable<Type>;

template <OutIterator Type>
void outFun(Type tp)
{}

int main()
{
    inFun(Iterable{});
    outFun(Iterable{});
}
