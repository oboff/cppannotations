    template <typename Type>
    class reverse_iter
    {
        public:
            explicit reverse_iter(Type *x)
            :
                current(x)
            {}
            Type &operator*() const
            {
                Type
                    *tmp = current;
                return (*--tmp);
            }
            Type *operator->() const
            {
                return &(operator*());
            }
            reverse_iter<Type>& operator++()
            {
                --current;
                return (*this);
            }
            reverse_iter<Type> operator++(int)
            {
                reverse_iter<Type>
                    tmp(current--);
                return (tmp);
            }
            bool operator!=(reverse_iter<Type> const &other)
            {
                return (current != other.current);
            }
        private:
            Type
                *current;
    };

    using reverse_iterator = reverse_iter<Type>;

    inline reverse_iterator rbegin()
    {
        return reverse_iterator(finish);
    }
    inline reverse_iterator rend()
    {
        return reverse_iterator(data);
    }
