#include <memory>
#include <iostream>

using namespace std;

//code
    struct Type
    {
        ~Type()
        {
            cout << "destr\n";  // show the object's destruction
        }
    };

    int main()
    {
        shared_ptr<Type[]> sp{ new Type[3] };
        sp[0] = sp[1];
    }
//=
#endif
