#ifndef INCLUDED_START_
#define INCLUDED_START_

#include "../../promisebase/promisebase.h"

#include "../awaitable/awaitable.h"

struct Start
{
    struct State: public PromiseBase<Start, State>
    {};

    using Handle = std::coroutine_handle<State>;

    std::coroutine_handle<State> d_handle;

    public:
        using promise_type = State;

        explicit Start(Handle handle);
        ~Start();

        Handle handle() const;
        void go();

        char const *name() const;
};

inline Start::Handle Start::handle() const
{
    return d_handle;
}

inline char const *Start::name() const
{
    return "Start";
}

extern Start g_start;

#endif
