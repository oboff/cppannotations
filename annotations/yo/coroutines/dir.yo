Here a class tt(Dir) is developed (recursively) showing all entries
in and below a specified directory. The program defines a class tt(Dir),
used by tt(main):
    verbinsert(-s4 //main demo/dir/main.cc)

The class tt(Dir), like the coroutine based implementation in the next
section, uses the tt(`dirent') bf(C) struct. As we prefer typenames starting
with capitals, tt(Dir) specifies a simple tt(using DirEntry = dirent)
so bf(C)'s typename doesn't have to be used.

tt(Dir) defines just a few data members: 
    tt(d_dirPtr) stores the pointer returned by bf(C)'s function tt(opendir);
    tt(d_recursive) points to a tt(Dir) entry that's used to handle a
        sub-directory of the current directory;
    tt(d_entry) is the name of the directory
        returned by tt(Dir::entry) member, which is refreshed at each call;
    tt(d_path) stores the name of the directory visited by a tt(Dir) object;
    and tt(d_entryPath) is tt(d_entry's) path name, starting at the initial
        directory name.
    Here is tt(Dir's) class interface:
    verbinsert(-s4 //hdr demo/dir/dir/dir.h)

tt(Dir's) constructor prepares its object for inspection of the entries of the
directory whose name is received as its argument: it calls
tt(opendir) for that directory, and prepares its tt(d_path) data member:
        verbinsert(-s4 //dir demo/dir/dir/dir1.cc)
    Once a tt(Dir) object's lifetime ends its destructor simply calls
tt(closedir) to return the memory allocated by tt(opendir):
    verbinsert(-s4 //dir demo/dir/dir/dir.h)

The member tt(entry) performs two tasks: first, if a recursion is active then
em(if) a recursive entry is available, that entry is returned. Otherwise, if
no recursive entry is available tt(d_recursive's) memory is deleted, and
tt(d_recursive) is set to 0:
    verbinsert(//first demo/dir/dir/entry.cc)

The second part is executed if there's no recursion or once all the recursive
entries have been obtained. In that case all entries of the current directory
are retrieved, skipping the two mere-dot entries. If the thus obtained entry
is the name of a directory then tt(d_recursive) stores the address of a newly
allocated tt(Dir) object (which is then handled at tt(Dir::entry's) next call)
and the just received entry name is returned:
    verbinsert(//second demo/dir/dir/entry.cc)

The member tt(Dir::entry) itself consists of these two parts, returning zero
(no more entries) once the second part's while-loop ends:
    verbinsert(-s4 //+entry demo/dir/dir/entry.cc)

Thus, the tt(class Dir) essentially requires one single member function, using
recursion to visit all directory entries that exist in or below the specified
starting directory. All sources of this program are available in the
distribution's tt(yo/coroutines/demo/dir) directory.

