Since the introduction of header files in the bf(C) language header files have
been the main tool for declaring elements that are not defined but are used 
in source files. E.g., when using tt(printf) in tt(main) the preprocessor
directive tt(#include <stdio.h>) had to be specified.

Header files are still extensively used in bf(C++), but gradually some
drawbacks emerged. One minor drawback is that, in bf(C++), header files
frequently not merely contain function and variable declarations but often
also type definitions (like class interfaces and enum definitions). When
designing header files the software engineer therefore commonly distinguishes
headers which are merely going to be used inside a local (class) context (like
the internal-header approach advocated in the annotations()) and header files
which are used externally. Those latter header files need some sort of include
guard to prevent them from being processed repeatedly by sources (indirectly)
including them. A more important drawback is that a header file has to be
processed again for every source file including it. Such a task is not a
trivial one. E.g., if a header file includes tt(iostream) and tt(string) then
that forces a compiler like  tt(g++ 11.2.0) to process over 750,000 bytes of
code for every source file including that header. 

To speed up compilations precompiled headers hi(header: precompiled) were
introduduced. Although the binary format of precompiled headers does indeed
allow the compiler to parse the contents of header files much faster than
their standard text format, they also are very large. A precompiled header
that merely includes tt(iostream) and tt(string) exceeds 20 MB: a bit more
than their text-file equivalent of about 750,000 bytes....

Modules were introduced to avoid the abovementioned complications. Although
modules can still include header files, once a module has been designed it
doesn't contain a single header file anymore, and consequenly programs that
merely use modules are compiled much faster than corresponding programs still
using headers.

There is another, conceptual, advantage associated with using modules. The
initial high-level programming languages (like Fortran and Algol), as well as
assembly languages provided functions (a.k.a. subroutines and procedures) to
distinguish conceptually different task levels, where each level is
implemented by a function. A program reading its data, then processing the
data and showing the final results could easily be designed:
        verb(    int main()
    {
        readData();
        processData();
        showResults();
    }
        )
    These functions often themselves often use (deeper) aggregation level
functions, using separate functions performing sub-tasks, etc, etc,
until trivial decomposition levels were reached which could cleary be
implemented with standard flow control and expression statements.

This decomposition methodology works very good. It still does. But at the
em(global) level there's a problem: there's little integrity
protection. Function parameters may help to maintain the program's data
integrity, but it's difficult to ensure the integrity of global data.

In this respect classes do a better job. Their tt(private) sections offer a
means for class-designers to guarantee the integrity of the classes' data.

In that sense modules allow us to take the next step up the (separation or
integrity) ladder. Conceptually modules provide program sections which are
completely separated from the rest of the program. Modules define what the
outside world can use and reach, whether variables, functions, or types (like
classes) and that's it. In that sense modules resemble factories: visitors can
go to showrooms and meeting halls, but the locations where the actual products
are being designed and constructed are not open to the public.

In this chapter we cover the current definition and realization of modules
according to the C++20 standard. To use modules with the current edition of the
Gnu tt(g++) compiler (version 12.0.1) requires the tt(--std=c++20) (or more
recent) standard specification as well as the hi(module flag) module
compilation flag ti(-fmodules-ts). E.g.,
        verb(
    g++ -c --std=c++20 -fmodules-ts -Wall modsource.cc
        )
